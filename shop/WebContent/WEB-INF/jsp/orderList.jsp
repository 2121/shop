<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)http://localhost:8080/mango/cart/list.jhtml -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

<title>订单页面</title>
<link href="${pageContext.request.contextPath}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/css/cart.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/css/product.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<div class="container header">
	<div class="span5">
		<div class="logo">
			<a href="index">
				<img src="${pageContext.request.contextPath}/image/r___________renleipic_01/logo.jpg" width="223" height="45" alt="我的商城"/>
			</a>
		</div>
	</div>
	<div class="span9">
<div class="headerAd">
	<img src="${pageContext.request.contextPath}/image/header.jpg" width="320" height="50" alt="正品保障" title="正品保障"/>
</div>	
</div>

		<%@ include file="menu.jsp" %>
	
</div>	

<div class="container cart">

		<div class="span24">
		
				<table>
					<tbody>
						<s:iterator value="OrderPageBean.list" var="o">
							<tr>
								<th colspan="5">
									订单编号:<s:property value="#o.oid"/>&nbsp;&nbsp;&nbsp;&nbsp;
									订单状态：
									<s:if test="#o.state==1">
										<a href="${pageContext.request.contextPath }/order_findByOid.action?oid=<s:property value="#o.oid"/>"><font color="red">未付款</font></a>
									</s:if>
									<s:if test="#o.state==2">
										<a href="#"><font color="red">付款未发货</font></a>
									</s:if>
									<s:if test="#o.state==3">
										<a href="#"><font color="red">发货未确认</font></a>
									</s:if>
									<s:if test="#o.state==4">
										<a href="#"><font color="red">交易完成</font></a>
									</s:if>&nbsp;&nbsp;&nbsp;&nbsp;
									订单时间:
									<s:property value="#o.ordertime" />
								
								</th>
							</tr>
							<tr>
								<th>图片</th>
								<th>商品</th>
								<th>价格</th>
								<th>数量</th>
								<th>小计</th>
							</tr>
							<s:iterator value="#o.orderItems" var="item">
								<tr>
									<td width="60">
										<input type="hidden" name="id" value="22"/>
										<img src="${pageContext.request.contextPath }/<s:property value="#item.product.image"/>"/>
									</td>
									<td>
										<a target="_blank"><s:property value="#item.product.pname"/></a>
									</td>
									<td>
										<s:property value="#item.product.shop_price"/>
									</td>
									<td class="quantity" width="60">
										<input type="text" name="count" value="<s:property value="#item.count"/>" maxlength="4" onpaste="return false;"/>
										<div>
											<span class="increase">&nbsp;</span>
											<span class="decrease">&nbsp;</span>
										</div>
									</td>
									<td width="140">
										<span class="subtotal">￥<s:property value="#item.subtotal"/></span>
									</td>
									
								</tr>
							</s:iterator>
						</s:iterator>
				</tbody>
			</table>
<div class="pagination">
			<span>第<s:property value="OrderPageBean.page"/>/<s:property value="OrderPageBean.totalPage"/>页</span>
				<s:if test="OrderPageBean.page!=1">
					<a href="${pageContext.request.contextPath }/order_findByUid.action?currentPage=1" class="firstPage"> &nbsp;</a>
					<a href="${pageContext.request.contextPath }/order_findByUid.action?currentPage=<s:property value='pageBean.Page-1'/>" class="previousPage"> &nbsp;</a>
				</s:if>
				
				<s:iterator var="i" begin="1" end="OrderPageBean.totalPage">
					<s:if test="OrderPageBean.page==#i">
						<span class="currentPage"><s:property value="#i"/></span>
					</s:if>	
					<s:elseif test="OrderPageBean.page!=#i">
						<a href="${pageContext.request.contextPath }/order_findByUid.action?currentPage=<s:property value='#i'/>"><s:property value="#i"/></a>
					</s:elseif>	
				</s:iterator>
				<s:if test="OrderPageBean.page!=OrderPageBean.totalPage">
					<a class="nextPage" href="${pageContext.request.contextPath }/order_findByUid.action?currentPage=<s:property value='OrderPageBean.Page+1'/>" class="previousPage">&nbsp;</a>
					
					<a class="lastPage" href="${pageContext.request.contextPath }/order_findByUid.action?currentPage=<s:property value='OrderPageBean.totalPage'/>" class="previousPage">&nbsp;</a>
				</s:if>
</div>
				
		
		</div>
		
	</div>
<div class="container footer">
	<div class="span24">
		<div class="footerAd">
					<img src="image\r___________renleipic_01/footer.jpg" alt="我们的优势" title="我们的优势" height="52" width="950">
</div>
</div>
	<div class="span24">
		<ul class="bottomNav">
					<li>
						<a href="#">关于我们</a>
						|
					</li>
					<li>
						<a href="#">联系我们</a>
						|
					</li>
					<li>
						<a href="#">诚聘英才</a>
						|
					</li>
					<li>
						<a href="#">法律声明</a>
						|
					</li>
					<li>
						<a>友情链接</a>
						|
					</li>
					<li>
						<a target="_blank">支付方式</a>
						|
					</li>
					<li>
						<a target="_blank">配送方式</a>
						|
					</li>
					<li>
						<a >SHOP++官网</a>
						|
					</li>
					<li>
						<a>SHOP++论坛</a>
						
					</li>
		</ul>
	</div>
	<div class="span24">
		<div class="copyright">Copyright © 2005-2015 网上商城 版权所有</div>
	</div>
</div>
</body>
</html>