package cn.itcast.shop.cart.action;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.Action;

import cn.itcast.shop.cart.vo.Cart;
import cn.itcast.shop.cart.vo.CartItem;
import cn.itcast.shop.product.service.ProductService;
import cn.itcast.shop.product.vo.Product;

public class CartAction implements Action {
	
	private Integer pid;
	
	private Integer count;
	
	private ProductService productService;
	
	
	
	
	

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String addCart() throws Exception {
		
		
		

		Cart cart =(Cart) ServletActionContext.getRequest().getSession().getAttribute("cart");
		
		if( cart ==null){
			
			 cart = new Cart();
			
			
		}
		
		Product product = productService.findByPid(pid);
		
		CartItem cartItem = new CartItem();
		
		cartItem.setCount(count);
		cartItem.setProduct(product);
		
		cart.addCart(cartItem);
		
		 ServletActionContext.getRequest().getSession().setAttribute("cart", cart);
		
		return "addCart";
	}
	public String removeCart() throws Exception {
		
		Cart cart =(Cart) ServletActionContext.getRequest().getSession().getAttribute("cart");
		
		cart.removeCart(pid);
		
		return "removeCart";
	}
	public String clearCart() throws Exception {
		
		Cart cart =(Cart) ServletActionContext.getRequest().getSession().getAttribute("cart");
		
		cart.getMaps().clear();
		
		return "clearCart";
	}
	
	
	public String myCart(){
		
		
		return "myCart";
	}
	
	
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
