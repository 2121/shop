package cn.itcast.shop.cart.vo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.LinkedMap;

public class Cart {
	
	private Map<Integer,CartItem> maps = new LinkedMap();
	
	private double total;
	
	
	
	public Map<Integer, CartItem> getMaps() {
		return maps;
	}



	public double getTotal() {
		return total;
	}



	public void setTotal(double total) {
		this.total = total;
	}



	public Collection<CartItem> getCartItem(){
		
		
		return maps.values();
	}
	
	
	
	public void addCart(CartItem cartItem){
		
		int pid = cartItem.getProduct().getPid();
		
		if( maps.containsKey(pid)){
			
			CartItem _cartItem = maps.get(pid);
			
			cartItem.setCount(cartItem.getCount() + _cartItem.getCount());
			
			total += cartItem.getSubtotal();
			
			maps.put(pid, cartItem);
		}else{
			
			total += cartItem.getSubtotal();
			maps.put(pid, cartItem);
		}
		
	}
	
	public void removeCart(int pid){
		
		CartItem cartItem = maps.remove(pid);
		
		total -=cartItem.getSubtotal();
	}
	

}
