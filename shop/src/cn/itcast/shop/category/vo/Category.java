package cn.itcast.shop.category.vo;

import java.util.Set;

import cn.itcast.shop.categorysecond.vo.Categorysecond;

public class Category {
	
	private Integer cid;
	private String cname;
	
	private Set<Categorysecond> categoryseconds;
	
	
	
	public Set<Categorysecond> getCategoryseconds() {
		return categoryseconds;
	}
	public void setCategoryseconds(Set<Categorysecond> categoryseconds) {
		this.categoryseconds = categoryseconds;
	}
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}

}
