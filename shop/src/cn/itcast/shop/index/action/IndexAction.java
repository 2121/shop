package cn.itcast.shop.index.action;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cn.itcast.shop.category.service.CategoryService;
import cn.itcast.shop.category.vo.Category;
import cn.itcast.shop.product.service.ProductService;
import cn.itcast.shop.product.vo.Product;

	/**
	 * 首页访问的Action
	 * @author zkr
	 *
	 */
public class IndexAction extends ActionSupport{

	private CategoryService categoryService;
	private ProductService productService;
	
	
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}


		public void setCategoryService(CategoryService categoryService) {
			
			this.categoryService = categoryService;
		}


		@Override
		public String execute() throws Exception {
			// TODO Auto-generated method stub
			
			
			
			
			//查找出所有一级分类
			List<Category> cList = categoryService.findAll();
			
			
			ActionContext.getContext().getSession().put("cList", cList);
			
			//查找出热门商品
			List<Product> hList = productService.findHProduct();
			
			ActionContext.getContext().getSession().put("hList", hList);
			
			//查找出最新商品
			List<Product> nList = productService.findNProduct();
			
			ActionContext.getContext().getSession().put("nList", nList);
			
			
		
			
	
				
			
	
			
			
			return "index";
		}
	
	

}
