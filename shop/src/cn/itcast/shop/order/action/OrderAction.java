package cn.itcast.shop.order.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.cart.vo.Cart;
import cn.itcast.shop.cart.vo.CartItem;
import cn.itcast.shop.order.service.OrderService;
import cn.itcast.shop.order.vo.Order;
import cn.itcast.shop.order.vo.OrderItem;
import cn.itcast.shop.user.vo.User;
import cn.itcast.shop.util.PageBean;

public class OrderAction extends ActionSupport implements ModelDriven<Order>{

	private OrderService orderService;
	
	private int currentPage;
	
	

	public int getCurrentPage() {
		return currentPage;
	}


	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	private Order order = new Order();

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
	

	@Override
	public Order getModel() {
		// TODO Auto-generated method stub
		return order;
	}
	
	
	public String save(){
		
		  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  
		 String date= format.format(new Date());
		
		try {
			order.setOrdertime( format.parse(date)  );
		} catch (ParseException e) {
			e.printStackTrace();
		}
		order.setState(1);   //1.未付款    2.付款未发货     3.发货未确认    4.交易完成
		
		
		User existUser = (User) ServletActionContext.getRequest().getSession().getAttribute("existUser");
		
		if( existUser ==null){
			
			addActionMessage("用户还没有登录！");
			return "login";
		}
		order.setUser(existUser);
		
		order.setAddr(existUser.getAddr());
		order.setName(existUser.getName());
		order.setPhone(existUser.getPhone());
		
		Cart cart = (Cart) ServletActionContext.getRequest().getSession().getAttribute("cart");
		
		if( cart == null){
			
			addActionMessage("您还没有购物，请先去购物！");
			
			return "msg";
			
		}
		
		order.setTotal(cart.getTotal());
		

		
		for(CartItem item : cart.getCartItem()){
			
			OrderItem orderItem = new OrderItem();
			
			orderItem.setCount(item.getCount());
			orderItem.setProduct(item.getProduct());
			orderItem.setSubtotal(item.getSubtotal());
			orderItem.setOrder(order);
			
			order.getOrderItems().add(orderItem);
		
			
		}
		
		
		orderService.save(order);
		
		
		cart.getMaps().clear();
		
		return "order";
		
	}
	
	public String findByUid(){
		
	User user = (User) ServletActionContext.getRequest().getSession().getAttribute("existUser");
		
	PageBean<User> pageBean  = orderService.findUid(user.getUid(),currentPage);
	
	ActionContext.getContext().getValueStack().set("OrderPageBean", pageBean);
	

		
		return "orderlist";
	}

	public String findByOid(){
		
		 order = orderService.findByOid(order.getOid());
		
		System.err.println(order.getAddr());
		return "order";
	}



}
