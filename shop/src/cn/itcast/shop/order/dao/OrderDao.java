package cn.itcast.shop.order.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cn.itcast.shop.order.vo.Order;
import cn.itcast.shop.product.vo.Product;
import cn.itcast.shop.user.vo.User;
import cn.itcast.shop.util.PageBean;
import cn.itcast.shop.util.PageHibernateCallback;

public class OrderDao extends HibernateDaoSupport{

	public void save(Order order) {
		
		this.getHibernateTemplate().save(order);
		
	}

	public int findCount(int uid){
		
		String hql = "select count(*) from Order o where o.user.uid = ?";
		
		List<Long> list = this.getHibernateTemplate().find(hql, uid);
		
		if( list!=null && list.size() >0){
			
			return list.get(0).intValue();
		}
		return 0;
		
	}

	public List<Order> findOrderByUid(Integer uid,int begin,int limit) {
		
		String hql = "select o  from Order o where o.user.uid = ? order by ordertime desc";
		
		List<Order> list = this.getHibernateTemplate()
				.execute(new PageHibernateCallback<Order>(hql,new Object[]{uid}, begin, limit));
		
		

		
		if(list !=null && list.size() > 0){
			
			return list;
		}
		
		
		return null;
	}

	public Order findByOid(int oid) {
		
		/*String hql = "select o from Order o where o.oid=?";
		
		List<Order> list = this.getHibernateTemplate().find(hql, oid);
		
		if( list !=null && list.size()>0){
			
			return list.get(0);
			
		}
		
		return null;*/
		
		Order order = this.getHibernateTemplate().get(Order.class, oid);
		
		
		return order;
		
		
	}
	
	
	

}
