package cn.itcast.shop.order.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.order.dao.OrderDao;
import cn.itcast.shop.order.vo.Order;
import cn.itcast.shop.user.vo.User;
import cn.itcast.shop.util.PageBean;

@Transactional
public class OrderService {
	
	private OrderDao orderDao;

	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}

	public void save(Order order) {
		
		orderDao.save(order);
		
	}
	


	public PageBean<User> findUid(Integer uid,int currentPage) {
		
		PageBean pageBean = new PageBean();
		
		pageBean.setPage(currentPage);
		
		int limit = 3;
		pageBean.setLimit(limit);
		
		int totalCount = orderDao.findCount(uid);
		
		pageBean.setTotalCount(totalCount);
		
		int totalPage = (totalCount - 1 ) / limit + 1;
		
		pageBean.setTotalPage(totalPage);
		
		int begin = ( currentPage -1 )*limit;
		
		List<Order> list = orderDao.findOrderByUid(uid,begin,limit);
		
		pageBean.setList(list);
		
		
		
		return pageBean;
	}

	public Order findByOid(int oid) {
		
		System.err.println(oid);
		
		return orderDao.findByOid(oid);
	}

	
	

}
