package cn.itcast.shop.product.action;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.category.service.CategoryService;
import cn.itcast.shop.category.vo.Category;
import cn.itcast.shop.product.service.ProductService;
import cn.itcast.shop.product.vo.Product;
import cn.itcast.shop.util.PageBean;

public class ProductAction extends ActionSupport implements ModelDriven<Product>{

	private ProductService productService;
	
	private CategoryService categoryService;
	
	//��ǰҳ
	private int currentPage;
	
	private int cid;
	
	private int csid;
	
	
	
	
	
	
	public int getCsid() {
		return csid;
	}



	public void setCsid(int csid) {
		this.csid = csid;
	}



	public int getCid() {
		return cid;
	}



	public void setCid(int cid) {
		this.cid = cid;
	}



	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}



	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}



	Product product = new Product();
	
	@Override
	public Product getModel() {
		// TODO Auto-generated method stub
		return product;
	}



	public void setProductService(ProductService productService) {
		this.productService = productService;
	}



	public String findByPid() throws Exception {
		// TODO Auto-generated method stub
		
		product = productService.findByPid(product.getPid());
		
		boolean isList = ActionContext.getContext().getSession().containsKey("cList");
		
		if( !isList){
			
			List<Category> cList = categoryService.findAll();
			
			ActionContext.getContext().getSession().put("cList", cList);
		}
		
		
		
		
		return "findByPidproPage";
	}
	
	public String findByCid() throws Exception{
		
		
		PageBean<Product> pageBean = productService.findProductByCid(currentPage,cid);
		
		
		ActionContext.getContext().getValueStack().set("pageBean", pageBean);
		
		
		return "productlist";
	}
	
	public String findByCsid() throws Exception{
		
		PageBean<Product> pageBean = productService.findByCsid(currentPage,csid);
		
		ActionContext.getContext().getValueStack().set("pageBean", pageBean);
		return "productlist";
	}
	
}
