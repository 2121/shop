package cn.itcast.shop.product.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cn.itcast.shop.product.service.ProductService;
import cn.itcast.shop.product.vo.Product;
import cn.itcast.shop.util.PageHibernateCallback;

public class ProductDao extends HibernateDaoSupport{
	
	private ProductService productSerivce;

	public void setProductSerivce(ProductService productSerivce) {
		this.productSerivce = productSerivce;
	}

	public List<Product> findHProduct() {
		// TODO Auto-generated method stub
		//分页的第一种写法
		DetachedCriteria criteria = DetachedCriteria.forClass(Product.class);
		
		criteria.add(Restrictions.eq("is_hot", 1));
		criteria.addOrder(Order.desc("pdate"));
		
		@SuppressWarnings("unchecked")
		List<Product> list = this.getHibernateTemplate().findByCriteria(criteria, 0, 10);
		
		return list;
		
		
	}

	public List<Product> findNProduct() {
		// TODO Auto-generated method stub
		
		DetachedCriteria criteria =  DetachedCriteria.forClass(Product.class);
		
		List<Product> list=this.getHibernateTemplate().findByCriteria(criteria, 0, 10);
		
		return list;
	}
	
	public Product fndByPid(Integer pid) {
		// TODO Auto-generated method stub
		
		Product product = this.getHibernateTemplate().get(Product.class, pid);
		
		
		return product;
		
		
	}
	//根据分类cid查询商品的个数
	public int findCountCid(Integer cid){
		
		String hql = "select count(*) from Product p where p.categorysecond.category.cid =?";
		
		List<Long> list = this.getHibernateTemplate().find(hql, cid);
		
		if( list !=null && list.size()>0){
			
			return list.get(0).intValue();
			
		}
		return 0;
	}
	//根据分类id查询商品的集合
	
	public List<Product> findByPageCid(Integer cid , int begin,int limit){
		
//select p.* from category c,categorysecond cs,product where c.cid = cs.csid and cs.csid = c.cid =2;
		
		String hql = "select p from Product p join p.categorysecond cs join  cs.category c where c.cid=?";
		//分页另一种写法
		List<Product> list = this.getHibernateTemplate().execute(new PageHibernateCallback<Product>(hql,new Object[]{cid}, begin, limit));
		
		if( list !=null && list.size() >0){
			
			
			return list;
		}
		
		return null;
	}

	public int findCountCsid(int csid) {
		
		String hql = "select count(*) from Product p where p.categorysecond.csid = ?";
		
		List<Long> list = this.getHibernateTemplate().find(hql, csid);
		
		if( list !=null && list.size()>0){
			
			return list.get(0).intValue();
			
		}
		return 0;
	}

	public List findByPageCsid(int csid, int begin, int limit) {
		
		String hql = "select p from Product p join p.categorysecond cs where cs.csid=?";
		//分页另一种写法
		List<Product> list = this.getHibernateTemplate().execute(new PageHibernateCallback<Product>(hql,new Object[]{csid}, begin, limit));
		
		if( list !=null && list.size() >0){
			
			
			return list;
		}
		
		return null;
	}
	
	
	
}
