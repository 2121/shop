package cn.itcast.shop.product.service;

import java.util.List;

import cn.itcast.shop.category.vo.Category;
import cn.itcast.shop.order.vo.Order;
import cn.itcast.shop.product.dao.ProductDao;
import cn.itcast.shop.product.vo.Product;
import cn.itcast.shop.util.PageBean;
import cn.itcast.shop.util.PageHibernateCallback;

public class ProductService {
	
	private ProductDao productDao;

	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}



	public List<Product> findHProduct() {
		// TODO Auto-generated method stub
		
		List<Product> list = productDao.findHProduct();
		
		if(list !=null && list.size()>0 ){
			
			return list;
		}
		
		
		return null;
	}



	public List<Product> findNProduct() {
		// TODO Auto-generated method stub
		List<Product> list =productDao.findNProduct();
		
		if ( list !=null && list.size()>0){
			
			return list;
			
		}
		
		return null;
	}


	//通过id查找一个商品
	public Product findByPid(Integer pid) {
		
		return productDao.fndByPid(pid);
		
	}



	public PageBean findProductByCid(int currentPage, int cid) {
		
		PageBean pageBean = new PageBean();
		
		pageBean.setPage(currentPage);
		
		int totalCount = productDao.findCountCid(cid);
		pageBean.setTotalCount(totalCount);
		int limit =8;
		pageBean.setLimit(8);
		
		int totalPage = (totalCount - 1 )/ limit +1;
		
		pageBean.setTotalPage(totalPage);
		
		int begin = (currentPage - 1) * limit;
		
		pageBean.setList(productDao.findByPageCid(cid, begin, limit));
				return pageBean;
	}



	public PageBean findByCsid(int currentPage, int csid) {
	PageBean pageBean = new PageBean();
		
		pageBean.setPage(currentPage);
		
		int totalCount = productDao.findCountCsid(csid);
		pageBean.setTotalCount(totalCount);
		int limit =8;
		pageBean.setLimit(8);
		
		int totalPage = (totalCount - 1 )/ limit +1;
		
		pageBean.setTotalPage(totalPage);
		
		int begin = (currentPage - 1) * limit;
		
		
		pageBean.setList(productDao.findByPageCsid(csid, begin, limit));
		
		
		return pageBean;
	}
	
	

}
