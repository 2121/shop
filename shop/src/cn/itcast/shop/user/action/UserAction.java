package cn.itcast.shop.user.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.http.HttpRequest;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.category.service.CategoryService;
import cn.itcast.shop.category.vo.Category;
import cn.itcast.shop.product.service.ProductService;
import cn.itcast.shop.product.vo.Product;
import cn.itcast.shop.user.service.UserSerivice;
import cn.itcast.shop.user.vo.User;
import cn.itcast.shop.util.MailUtils;

public class UserAction extends ActionSupport implements ModelDriven<User>{
	
	private UserSerivice userservice;
	private CategoryService categoryService;
	private ProductService productService;
	
	
	
	
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	private String checkCode;
	
	
   

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public void setUserservice(UserSerivice userservice) {
		this.userservice = userservice;
	}

	public User user=new User(); 
    
	public String registPage(){
		
		//判断是否session中否包含一级分类
		boolean isList = ActionContext.getContext().getSession().containsKey("cList");
		
		if( !isList){
			
			List<Category> cList = categoryService.findAll();
			
			ActionContext.getContext().getSession().put("cList", cList);
		}
		
		
		return "registPage";
	}
	
	public String loginPage(){
		

	//判断是否session中否包含一级分类
	boolean isList = ActionContext.getContext().getSession().containsKey("cList");
		if( !isList){
			
			List<Category> cList = categoryService.findAll();
			
			
			
			ActionContext.getContext().getSession().put("cList", cList);
		}
		
		return "login";
	}
	
	  @Override  
	public User getModel() {  
	        // TODO Auto-generated method stub  
	   return user;  
	} 
	
	public String checkUsername() throws IOException{
		
		User existUser = userservice.checkUsername(user.getUsername());
		
		HttpServletResponse response = ServletActionContext.getResponse();
		
		response.setContentType("text/html;charset=UTF-8");
		
		if(existUser != null){
			
			response.getWriter().println("<font color='red'>用户名已经存在</font>");
			
		}else{
			
			response.getWriter().println("<font color='green'>用户名可以使用</font>");
		}
		return NONE;
		
	}
	
	public String regist(){
		
		userservice.save(user);
		
		MailUtils.sendMail(user.getEmail(), user.getCode());
		
		//清空错误信息
		clearErrorsAndMessages();  
		addActionMessage("注册成功！请到邮箱中激活！");
		
		
		return "msg";
	}
	
	//用户激活
	public String active(){
		
		User existUser = userservice.findByCode(user.getCode());
		
		if( existUser !=null){
			
			addActionMessage("用户激活成功：请登录");
			existUser.setCode(null);
			existUser.setState(1);
			
			userservice.update(existUser);
			
			
		}else{
			
			addActionMessage("激活失败！");
		}
		
		return "msg";
		
	}
	
	//用户激活
	public String login(){
		
		User existUser = userservice.login(user);
		//生成的图片
		String checkcode =(String) ServletActionContext.getRequest().getSession().getAttribute("checkcode");
		
		if( !checkcode.equalsIgnoreCase(this.checkCode)){
			addActionMessage("验证码不正确！");
			
			return ERROR;
			
		}
		boolean iscList = ActionContext.getContext().getSession().containsKey("cList");
		if( !iscList){
			
			List<Category> cList = categoryService.findAll();
			ActionContext.getContext().getSession().put("cList", cList);
		}
		
		//查找出热门商品
		
		boolean ishList = ActionContext.getContext().getSession().containsKey("hList");
		
		if( !ishList){
				
			List<Product> hList = productService.findHProduct();
			
			ActionContext.getContext().getSession().put("hList", hList);
		}
		
		boolean isnList = ActionContext.getContext().getSession().containsKey("nList");
		
		//查找出最新商品
		if( !isnList){
			List<Product> nList = productService.findNProduct();
			
			ActionContext.getContext().getSession().put("nList", nList);
		}
		
		if(existUser!=null){
			
			ServletActionContext.getRequest().getSession().setAttribute("existUser", existUser);
			
			return LOGIN;
		}
		clearErrorsAndMessages();
		addActionMessage("用户名不正确或密码不正确或用户还没有激活");
		
		
		
		return ERROR;
		
	}

	public String ligout(){
		
		//ServletActionContext.getRequest().getSession().invalidate();
		
		ServletActionContext.getRequest().getSession().setAttribute("existUser", null);
			
		
		
		return SUCCESS;
	}



	
}
