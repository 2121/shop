package cn.itcast.shop.user.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cn.itcast.shop.user.vo.User;
import cn.itcast.shop.util.UUIDUtil;

public class UserDao extends HibernateDaoSupport{
	
	public User findByUsername(String username){
		
		String hql="from User where username=?";
		
		
		
		List<User> list = this.getHibernateTemplate().find(hql, username);
		
		
		
		if( list != null && list.size()>0){
			return list.get(0);
		}
		return null;
		
	}

	public void save(User user) {
		// TODO Auto-generated method stub
		
		//����״̬
		user.setState(0);
		
		user.setCode(UUIDUtil.getUUID());
		
		
		this.getHibernateTemplate().save(user);
		
	}

	public User fnndByCode(String code) {
		// TODO Auto-generated method stub
		
		String hql = "from User where code=?";
		
		 List<User> list = this.getHibernateTemplate().find(hql, code);
		 
		 if( list != null && list.size()>0){
				return list.get(0);
			}
			return null;
		
	}

	public void update(User user) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().update(user);
	}

	public User login(User user) {
		// TODO Auto-generated method stub
		
		String hql = "from User where username=? and password=? and state=1";
		
		List<User> list = this.getHibernateTemplate().find(hql, user.getUsername(),user.getPassword());
		
		if(list !=null&&list.size()>0){
			
			return list.get(0);
		}
		return null;
	}
	

}
