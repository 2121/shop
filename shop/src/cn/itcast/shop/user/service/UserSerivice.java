package cn.itcast.shop.user.service;

import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.user.dao.UserDao;
import cn.itcast.shop.user.vo.User;
@Transactional
public class UserSerivice {
	
	private UserDao userdao;
	
	public void setUserdao(UserDao userdao) {
		this.userdao = userdao;
	}

	public UserDao getUserdao() {
		return userdao;
	}

	public User checkUsername(String username){
		
		return userdao.findByUsername(username);
	}

	public void save(User user) {
		// TODO Auto-generated method stub
		
		userdao.save(user);
		
	}

	public User findByCode(String code) {
		// TODO Auto-generated method stub
		
		return userdao.fnndByCode(code);
		
	}

	public void update(User user) {
		// TODO Auto-generated method stub
		
		userdao.update(user);
		
	}

	public User login(User user) {
		// TODO Auto-generated method stub
		
		User existUser = userdao.login(user);
		if(existUser!=null){
			
			return existUser;
		}
		
		return null;
	}
}
