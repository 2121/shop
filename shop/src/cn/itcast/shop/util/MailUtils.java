package cn.itcast.shop.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailUtils {
	
	public static void sendMail(String to , String code){
		
		/**
		 * 1.获得一个Session对象.
		 * 2.创建一个代表邮件的对象Message.
		 * 3.发送邮件Transport
		 */
		// 1.获得连接对象
		Properties props = new Properties();
		props.setProperty("mail.host", "localhost");
		
		Session session = Session.getInstance(props, new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				// TODO Auto-generated method stub
				return new PasswordAuthentication("service@shop.com", "111");
			}
		});
		
		// 2.创建邮件对象:
		Message message = new MimeMessage(session);
		
		try {
			//设置收件人
			message.setFrom(new InternetAddress("service@shop.com"));
			//抄送  cc  密送Bcc
			//设置收件人
			message.addRecipient(RecipientType.TO, new InternetAddress(to));
			//设置邮件标题
			message.setSubject("来自购物天堂我的商城官方激活邮件");
			//设置邮件正文
			message.setContent("<h1>来自我的商城的官方激活邮件！点下面的链接完成激活!</h1><a href='http://localhost:8080/shop/user_active.action?code="+code+"'>http://localhost:8080/shop/user_active.action?code="+code+"</a>", "text/html;charset=utf-8");
			//3.发送邮件
			
			Transport.send(message);
		
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

	

}
